let tileBox = document.querySelector('#tile-box');
let imageArray = ['./images-dir/watermelon.png', './images-dir/strawberry.png', './images-dir/grapes.png', './images-dir/fruit.png', './images-dir/dried-fruits.png', './images-dir/cherries.png', './images-dir/avocado.png', './images-dir/apple.png', './images-dir/watermelon.png', './images-dir/strawberry.png', './images-dir/grapes.png', './images-dir/fruit.png', './images-dir/dried-fruits.png', './images-dir/cherries.png', './images-dir/avocado.png', './images-dir/apple.png'];
function shuffle(array) {
    for (let indexShuffle = array.length - 1; indexShuffle > 0; indexShuffle--) {
        const indexShuffle2 = Math.floor(Math.random() * (indexShuffle + 1));
        [array[indexShuffle], array[indexShuffle2]] = [array[indexShuffle2], array[indexShuffle]];
    }
    return array;
}


let imageIndexArray = [];
for (let index = 1; index <= 16; index++) {
    let tile = document.createElement("div");
    tile.setAttribute(`class`, `tile`);
    tile.innerHTML = `<img class='tile-images' id='tile${index}' src='./images-dir/back-face.png'></img>`;
    imageIndexArray.push(shuffle(imageArray)[0]);
    imageArray.splice(0, 1);
    tileBox.append(tile);
}

let count = 0;
let faceOne = '';
let faceTwo = '';
let moves = 0;
let matchedPairs = 0;

let timer;
let seconds = 0;
let minutes = 0;
let timerDisplay = document.getElementById('timer');
let movesDisplay = document.getElementById('moves');


document.addEventListener('click', function handleFlip(event) {
    event.preventDefault();
    let tempTile = document.getElementById(event.target.id);
    console.log(tempTile.id);
    if (tempTile.id === 'start-button') {

        window.location.href = './index.html';
    }
    // console.log(tempTile);
    let imageNum = (event.target.id).split('e')[1];
    // console.log(event.target.id);
    if (tempTile.classList.contains('tile-images')) {
        // console.log("heh");
        moves++;
        let currentImage = tempTile.src;
        let newImage = (currentImage.includes('back-face.png')) ? `${imageIndexArray[imageNum - 1]}` : './images-dir/back-face.png';
        tempTile.src = newImage;
        // console.log(tempTile.id);
        tempTile.classList.toggle('flip');

        if (faceOne === '') {
            faceOne = tempTile.id;
            console.log(faceOne);
        }
        else {
            faceTwo = tempTile.id;
            console.log(faceTwo);
        }

        count += 1;
        if (count == 2) {
            console.log("aaya");
            if (faceOne !== faceTwo) {

                setTimeout(() => {

                    console.log(document.getElementById(faceTwo).src);
                    if (document.getElementById(faceTwo).src === document.getElementById(faceOne).src && !document.getElementById(faceTwo).src.includes('/back-face.png')) {
                        console.log("LOL");

                        document.getElementById(faceOne).classList.toggle('matched');
                        matchedPairs++;
                        document.getElementById(faceTwo).classList.toggle('matched');
                        faceOne = faceTwo = '';
                        count = 0;
                        if(matchedPairs === 8)
                        {
                            document.getElementById('tile-box').textContent = 'Congratulations! You Won!'+'\n'+'Time taken:'+ seconds+' seconds'+'\n'+'Moves Used:'+ moves;
                            setTimeout(() => {
                                window.location.href = './index.html';
                            },5000);
                        }
                    }
                    else {
                        console.log('lol');
                        document.getElementById(faceOne).classList.toggle('flip');
                        document.getElementById(faceOne).src = './images-dir/back-face.png';
                        document.getElementById(faceTwo).classList.toggle('flip');
                        document.getElementById(faceTwo).src = './images-dir/back-face.png';

                        faceOne = faceTwo = '';
                        count = 0;
                    }
                }, 400);
            }
            else {
                faceOne = faceTwo = '';
                count = 0;
            }
        }
    }
    if (moves == 1) {
        startTimer();
        document.getElementById('start-button').style.color = 'gray';
    }
    movesDisplay.textContent = moves+' moves';
})
function startTimer() {
    timer = setInterval(() => {
        seconds++;
        // if (seconds === 60) {
        //     seconds = 0;
        //     minutes++;
        // }
        timerDisplay.textContent = 'time:' +seconds+' sec';
    }, 1000);
}
